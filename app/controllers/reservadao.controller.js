const db = require("../models");
const models = require("../models");
const Reserva = db.Reserva;
const Op = db.Sequelize.Op;
exports.create = (req, res) => {


    // Validate request
    if (!req.body.idMesa || !req.body.idResto || !req.body.idCliente || !req.body.rangoInferior || !req.body.rangoSuperior  ) {
        res.status(400).send({
            message: "Error falta el envio de un campo Obligatorio (Cliente, Mesa, Resto ó Rango Horaio)!"
        });
        return;
    }

    // Crea una Mea
    const reserva = {
        idMesa: req.body.idMesa,
        idResto: req.body.idResto,
        idCliente: req.body.idCliente,
        rangoSuperior: req.body.rangoSuperior,
        rangoInferior: req.body.rangoInferior,
        fecha: req.body.fecha
    };
    // Guardamos a la base de datos
    Reserva.create(reserva)
        .then(data => {
            res.send(data);

        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear la reserva."
            });
        });
};


// Update
exports.update = (req, res) => {
    const id = req.params.id;

    Reserva.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "La reserva se actualizó correctamente."
                });
            } else {
                res.send({
                    message: `No se puede actualizar la Reserva con id = ${id}. 
                    ¡Quizás no se encontró la Reserva o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al actualizar la Reserva con id=" + id
            });
        });
};

// Delete
exports.delete = (req, res) => {
    const id = req.params.id;

    Reserva.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "La reserva se elimino correctamente."
                });
            } else {
                res.send({
                    message: `No se puede eliminar la reserva con id = ${id}. 
                    ¡Quizás no se encontró la reserva o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al eliminar la reserva con id=" + id
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Reserva.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener la reserva con id=" + id
            });
        });
};

exports.findAll = (req, res) => {
    const name = req.query.name;
    const condition = name ? { idResto: { [Op.iLike]: `%${name}%` } } : null;

    Reserva.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener las reservas."
            });
        });
};

/*exports.findAll = (req, res) => {
    const name = req.query.name;
    const condition = name ? { idResto: { [Op.iLike]: `%${name}%` } } : null;

    Reserva.findAll({ attributes: ['id','idMesa', 'idResto', 'idCliente', 'fecha', 'rangoInferior', 'rangoSuperior'],
        include: [{
            model: models.Cliente,
            attributes: ['id', 'nombre','apellido']
        }],
        where: { idCliente: db.Cliente.idCliente } })
        .then(data => {
            res.send(data);
        })

        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener las reservas."
            });
        });
};
*/



