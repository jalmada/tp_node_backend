const db = require("../models");
const Cliente = db.Cliente;
const Op = db.Sequelize.Op;
exports.create = (req, res) => {


    // Validate request
    if (!req.body.cedula) {
        res.status(400).send({
            message: "Debe enviar el Número de Cedula!"
        });
        return;
    }

    // Crea un Cliente
    const cliente = {
        cedula: req.body.cedula,
        nombre: req.body.nombre,
        apellido: req.body.apellido
    };
    // Guardamos a la base de datos
    Cliente.create(cliente)
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear el Cliente."
            });
        });
};


// Update
exports.update = (req, res) => {
    const id = req.params.id;

    Cliente.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El cliente se actualizó correctamente."
                });
            } else {
                res.send({
                    message: `No se puede actualizar el Cliente con id = ${id}. 
                    ¡Quizás no se encontró el CLiente o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al actualizar el Cliente con id=" + id
            });
        });
};

// Delete
exports.delete = (req, res) => {
    const id = req.params.id;

    Cliente.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El cliente se elimino correctamente."
                });
            } else {
                res.send({
                    message: `No se puede eliminar el cliente con id = ${id}. 
                    ¡Quizás no se encontró el cliente o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al eliminar el cliente con id=" + id
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Cliente.findByPk(id)
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener el cliente con id=" + id
            });
        });
};
exports.findAll = (req, res) => {
    const name = req.query.name;
    const condition = name ? { nombre: { [Op.iLike]: `%${name}%` } } : null;

    Cliente.findAll({ where: condition })
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener los clientes."
            });
        });
};

