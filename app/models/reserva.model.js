module.exports = (sequelize, Sequelize) => {
    const Reserva = sequelize.define("Reservas", {
        idMesa: {
            type: Sequelize.STRING
        },
        idResto: {
            type: Sequelize.BIGINT
        },
        idCliente: {
            type: Sequelize.BIGINT
        },
        cliente: {
            type: Sequelize.STRING
        },
        fecha: {
            type: Sequelize.DATE
        },
        rangoInferior: {
            type: Sequelize.BIGINT
        },
        rangoSuperior: {
            type: Sequelize.BIGINT
        },
        capacidadSolicitada: {
            type: Sequelize.BIGINT,
            defaultValue: '1'
        },
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    }
    );



    return Reserva;
};
