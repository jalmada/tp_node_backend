module.exports = (sequelize, Sequelize) => {
    const Mesa = sequelize.define("Mesas", {
        nombreMesa: {
            type: Sequelize.STRING
        },
        idResto: {
            type: Sequelize.BIGINT
        },
        capacidad: {
            type: Sequelize.BIGINT
        },
        planta: {
            type: Sequelize.BIGINT,
            defaultValue: '1'
        },
        posX: {
            type: Sequelize.STRING
        },
        posY: {
            type: Sequelize.STRING
        },
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });
    return Mesa;
};