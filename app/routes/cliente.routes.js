module.exports = app => {
    const cliente = require("../controllers/clientedao.controller");
    const router = require("express").Router();
    router.post("/", cliente.create);
    router.put("/:id", cliente.update);
    router.delete("/:id", cliente.delete);
    router.get("/", cliente.findAll);
    router.get("/:id", cliente.findOne);
    app.use('/api/cliente', router);
};
